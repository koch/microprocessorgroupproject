# MicroprocessorGroupProject

## Installation
```
- cd to your assign02 subfolder in your pico-apps folder.
- git clone https://gitlab.scss.tcd.ie/koch/microprocessorgroupproject.git
```

Now a git repo has been made inside this folder and you can start creating a branch.
## Creating a new branch
To create a new branch to start working on your task do the following:
```
- git branch [branchname]
- git checkout [branchname]
```
Now your local repo has a branch called [branchname]
Once you have made changes to your branch , you can push the changes to the remote repo by doing the following:

```
- git status (Checks which files has been updated)
- git add . (Adds all the files to be staged)
- git commit -m "message" (fill in the "message" section with what a short message of what your update does)
- git push origin [branchname] (Pushes your changes to the remote branch)
```
