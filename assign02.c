#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include"hardware/watchdog.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "assign02.pio.h"

#define DASH_THRESHOLD 500000 // 0.5 seconds threshold to be considered a dash
#define numberOfAlphanums   35 // 10 numbers + 26 letters = 36 characters overall , - 1 for array indexing
#define maxMorseLength      4 // Maximum length of morse code string (0 - 4)

#define IS_RGBW true        // Will use RGBW format
#define NUM_PIXELS 1        // There is 1 WS2812 device in the chain
#define WS2812_PIN 28       // The GPIO pin that the WS2812 connected to

char selectedChar;          // Char Chosen for player to Type in morse code
char selectedCharMorse[5];  // Holds Char Chosen in morse code
char playerBuffer[5];       // Holds player input before submmitting

int charsEntered = 0;
int numLifes = 3;
int difficulty;
bool hasStarted = false;

int level_progress;

// Load alphanumerical characters into array
char alphaNumArr[] = {'0','1', '2', '3', '4', '5', '6', '7', '8','9','a','b','c','d',
                      'e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
                      'w','x', 'y','z'};
// Load morse code equivalent of alphanumberical characters into another array
char *morseArr[] = {"-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.",
                    ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--",
                    "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};

// Must declare the main assembly entry point before use.
void main_asm();

static inline void put_pixel(uint32_t pixel_grb) {
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}

static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b) {
    return  ((uint32_t) (r) << 8)  |
            ((uint32_t) (g) << 16) |
            (uint32_t) (b);
}


// Add Function to initialise pins
void asm_gpio_init(uint pin) {
    gpio_init(pin);
}

// Add function to set pin to be either input (buttons) or output (led)
void asm_gpio_set_dir(uint pin, bool out) {
    gpio_set_dir(pin, out);
}

// Enable falling-edge interrupt – see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_set_irq_falling(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL, true);
}

// Enable rising-edge interrupt – see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_set_irq_rising(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_RISE, true);
}

// Member Bhrajmot's Part
void set_watchdog_update(){
    watchdog_update();
}

/**
 * @brief Function to reset global variables using in morse code calculations
 * @author Chi
 * 
 */
void reset(){
    numLifes = 3;       // Number of lifes
    level_progress = 1; // Current Level Progress
    charsEntered = 0;   // chars entered into player buffer
    playerBuffer[0] = '\0'; // Reset the player buffer so new inputs can be read
    void light_green();
}

// Function Created by Swetang
//  random morse generator
//  generates number from 0-35
int rand_morse_gen(){
    int num;
    int upper_limit = numberOfAlphanums, lower_limit = 0;
    return num = (rand() % (upper_limit - lower_limit + 1)) +lower_limit;
}

/**
 * @brief Calculates the time spent holding button to determine morse code output
 * @author Chi 
 * @param time_held     Time held in Microseconds
 * @return int          Returns 1 or 0 used to determine if input is dot or dash
 */
int calculate_output(int time_held){
    if (time_held < DASH_THRESHOLD)
        return 0; // 0 For DOT
    else
        return 1; // 1 For DASH
}

/**
 * @brief           Reads input from button and appends morse code equivalent onto buffer
 * @author Chi
 * @param input     Int 1 or 0 passed from ASM and used to determine if 
 *                  dot or dash should be appended onto playerBuffer
 */
void input_handler(int input){
    if (charsEntered < 5){
        if (input == 0){
            printf(".");
            strcat(playerBuffer,".");
            charsEntered++;
            
        }else if (input == 1){
            printf("-");
            strcat(playerBuffer,"-");
            charsEntered++;
        }else{
            printf(" ");
        }
    }
}

// LED Code created by Shruti , Modified by Chi
// Function which change LED Colours
void light_green() {                                      
    put_pixel(urgb_u32(0x00, 0x7F, 0x00));                  
}
void light_blue() {
    put_pixel(urgb_u32(0x00, 0x00, 0x7F));                  
}
void light_yellow() {
    put_pixel(urgb_u32(0x7F, 0x7F, 0x00));                  
}
void light_orange() {
    put_pixel(urgb_u32(0x7F, 0x40, 0x00));                  
}
void light_red() {
   put_pixel(urgb_u32(0x7F, 0x00, 0x00));                 
}

void updateLifes(int lives){

    switch(lives){
        case 3: 
            light_green();   
            break;
        case 2:
            light_yellow();
            break;
        case 1:
            light_orange();
            break;
        case 0:
            light_red();   
            break;
    }
            
}

/**
 * @brief   Function used to generate a random alpha numeric character used for levels
 * 
 */
void generate_char(){
    int index = rand_morse_gen();               // Fetch random number between 0 - 35 for index
    selectedChar = alphaNumArr[index];          // Assigns selectedChar to a certain character in alphaNumerical array using index
    strcpy(selectedCharMorse,morseArr[index]);  // Stores morse code equivalent onto Array used to compare player buffer
    if (difficulty == 1)
        printf("Enter the Character: %c || Morse Code Equivalent: %s\n",selectedChar,selectedCharMorse);
    else
        printf("Enter the Character: %c || No Hints Provided.\n",selectedChar);
}   

/**
 * @brief   Function called by ASM Alarm0 after 2 seconds have passed since space input
 *          Primary function used to determine if player input matches the given character
 *          Also used to determine Lives and LED state and current level and stage
 * 
 */
void submit_answer() {
    if (hasStarted){
        
        bool isWrong = true;
        bool isFound = false;
        int foundIndex = 0;
        // Determine if User buffer matches any characters in alphanumberic array
        for(int i = 0; i <= numberOfAlphanums; i++ ){
            if (strcmp(playerBuffer, morseArr[i]) == 0){
                isFound = true;
                foundIndex = i;
                break;
            }
        }
        // Found and Character is Correct
        if (isFound && strcmp(playerBuffer,selectedCharMorse) == 0){
            if(numLifes < 3)
                numLifes++;
            printf("\nCorrect Output!");
            isWrong = false;
            level_progress++;
            playerBuffer[0] = '\0';
            charsEntered = 0;
        }else if (isFound){ // Found but not Character Wanted
            printf("\nYou have entered the wrong character.\n Desired Output: %c || Entered Output: %c \n",selectedChar,alphaNumArr[foundIndex]);
            numLifes--;
            playerBuffer[0] = '\0';
            charsEntered = 0;
        }else{  // Answer is just wrong
            printf("\nThe answer you entered is completely wrong. Output: ?\n");
            numLifes--;
            playerBuffer[0] = '\0';
            charsEntered = 0;
        }

        updateLifes(numLifes);      // Update LED to match number of lifes

        if (numLifes == 0){
            printf("\n\n****GAMEOVER****\n\n");
            hasStarted = false;
        }else {
            if(level_progress > 5 && difficulty == 2){
                printf("\n\nCongrats you beat the game!\n\n");
                hasStarted = false;
            }else if(level_progress > 5 ){
                printf("\nProceeding to Next level\n");
                level_progress = 1; 
                difficulty++;
                generate_char();
            }else if (!isWrong){
                printf("\nCurrent Difficulty: %d, Current Stage: %d\n",difficulty,level_progress);
                generate_char();
            }
        }
    }  
}  

// Function Created by Swetang
void group_info(){
    
    printf("\t @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    printf("\t  Group Number #18 And Team Members ");
    printf("\t  1-Ritwik Kumar                          \n");
    printf("\t  2-shruti Kathuria                       \n");
    printf("\t  3-Chi Chun Ko                           \n");
    printf("\t  4-swetang Krishna                       \n");
    printf("\t  5-Brahmjot Kaur                         \n");
    printf("\t  ----------------------------------------\n");
    printf("Enter .---- for level 1  (difficulty level easy - type Characters)\n");
    printf("Enter ..--- for level 2 (difficulty level easy  - type Characters)\n");
}
// Function Created by Ritwik, modified by Chi
int game_start(){ 
    
    //printf("Enter ...-- for level 3 (difficulty level hard  - type characters)\n");
    //printf("Enter ....- for level 4 (difficulty level hard   - type word)\n");
    if (charsEntered == 5){
        if (strcmp(playerBuffer,".----")==0)
        {
            difficulty = 1;
            reset();
            return 1;
        }
        else if(strcmp(playerBuffer, "..---")==0)
        {
            difficulty = 2;
            reset();
            return 1;
        }
        else
        {
            printf("Input does not match level. Please Try again.\n");
            playerBuffer[0] = '\0';
            charsEntered = 0;
            return 0;
        }
    }
    return 0;

}
/**
 * @brief       Entry Function used to determine which level the user plays.
 *              Watchdog Timer is started here.
 * @author Chi
 */
void level_selection(){
    // Takes current time and uses it as a random seed to generate random character to be selected.
    uint32_t time_seed;
    time_seed = time_us_32();
    srand(time_seed);

    if (difficulty == 1){
        printf("\n\n Entering.... Level #1: Individual characters with their equivalent Morse code provided.\n\n");
        // Generate random character
        int index = rand_morse_gen();
        selectedChar = alphaNumArr[index];
        strcpy(selectedCharMorse,morseArr[index]);
        
        reset();            // Reset Global Variables         
        hasStarted = true;  // Set flag for submission 
        printf("Enter the Character: %c || Morse Code Equivalent: %s\n",selectedChar,selectedCharMorse);
        watchdog_enable(9000, 1);   // Enable Watchdog
    }else if(difficulty == 2){
        printf("\n\n Entering.... Level #2: Individual characters without their equivalent Morse code provided.\n\n");
        // Generate random character
        int index = rand_morse_gen();
        selectedChar = alphaNumArr[index];
        strcpy(selectedCharMorse,morseArr[index]);
        // Reset all variables before heading into level
        reset();
        hasStarted = true;  // Set flag for submission 
        printf("Enter the Character: %c || No Hints Provided",selectedChar);
        watchdog_enable(9000, 1);   // Enable Watchdog
    }else{
        printf("Error occured during Level Selection\n");
    }
}
/*
 * Main entry point for the program
 */
int main() {
    stdio_init_all();       // Initialise all basic IO
    group_info(); 
    // Initialise the PIO interface with the WS2812 code
    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, IS_RGBW);
    light_blue();               // Set LED to Blue 

    // Jump into the main assembly code subroutine.
    main_asm();
    
    

    
    return(0);
}
